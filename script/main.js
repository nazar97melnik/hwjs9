"use strict";
// ## Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// за допомогою методу createElement(), після цього цей елемент потрібно помістити в сам документ методами append(),prepend(),before(),after(). йому також можна давати класи, атрибути або задавати текст який буде всередині цього тегу.
// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// перший параметр цієї функції означає де саме буде знаходитись елемент відносно елементу в який ми його вставляємо, цей параметр може мати такі значення : "beforeend"/"beforebegin"/"afterbegin"/"afterend"
// 3. Як можна видалити елемент зі сторінки?
// видалити елемент можна методом elem.remove() Або ще методоом replaceWith() цей метод замінить елемент новим елементом , при цьому видаливши старий елемент.


function writeElement(array,parent = document.body){
const ulList = document.createElement("ul");
parent.prepend(ulList)
    for(let key of array){
        const liElem = document.createElement("li");
        liElem.insertAdjacentText("afterbegin", key);
        ulList.append(liElem);
    };

}
writeElement(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);